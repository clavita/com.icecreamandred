<?php

require_once '../../../wp-load.php';

class ContentManager {

    static private $about_page_post_id      = 6;
    static private $catalogue_category_id   = 2;
    static private $news_category_id        = 5;
    static private $contact_us_post_id      = 24;

    static function __get_about_page() {

        $page = get_post(self::$about_page_post_id);

        $url = wp_get_attachment_url( get_post_thumbnail_id( self::$about_page_post_id ) );

        $response = array(
            'name'  => get_post_meta(self::$about_page_post_id,'name'),
            'text'  => $page->post_content,
            'image' => $url
        );

        return $response;
    }

    static function __get_categories() {

        $categories = get_categories(array(
            'child_of'      => self::$catalogue_category_id,
            'hide_empty'    => false,
        ));

        $clean_return = array();

        foreach ($categories as $category) {

            array_push($clean_return, array(
                'id'    => $category->term_id,
                'name'  => $category->name
            ));

        }

        return $clean_return;
    }

    static function __get_items($category = null) {

        $args = array(
            'posts_per_page'    => -1,
            'category'          => ($category ? $category : 2)
        );

        $items = get_posts($args);

        $clean_return = array();

        foreach ($items as $item) {

            $image_url = wp_get_attachment_url( get_post_thumbnail_id( $item->ID ) );

            array_push($clean_return, array(
                'name'          => $item->post_title,
                'price'         => get_post_meta($item->ID, 'price'),
                'materials'     => get_post_meta($item->ID, 'material'),
                'length'        => get_post_meta($item->ID, 'length'),
                'height'        => get_post_meta($item->ID, 'height'),
                'width'         => get_post_meta($item->ID, 'width'),
                'description'   => $item->post_content,
                'image'         => $image_url,
            ));

        }

        return $clean_return;
    }

    static function __get_news() {

        $args = array(
            'posts_per_page'    => -1,
            'category'          => self::$news_category_id
        );

        $items = get_posts($args);

        $clean_return = array();

        foreach ($items as $item) {

            $image_url = wp_get_attachment_url( get_post_thumbnail_id( $item->ID ) );

            array_push($clean_return, array(
                'title'         => $item->post_title,
                'date'          => $item->post_date,
                'author'        => get_post_meta($item->ID, 'author'),
                'text'          => $item->post_content,
                'image'         => $image_url,
            ));

        }

        return $clean_return;
    }

    static function __get_contact_us() {

        $response = array(
            'email'     => get_post_meta(self::$contact_us_post_id, 'email'),
            'address'   => get_post_meta(self::$contact_us_post_id, 'address'),
            'cell'      => get_post_meta(self::$contact_us_post_id, 'cell')
        );

        return $response;
    }

}