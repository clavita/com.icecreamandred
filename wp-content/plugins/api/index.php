<?php

require_once 'functions.php';

$response = null;

switch ($_REQUEST['function']) {

    case 'get_about_page':
        $response = ContentManager::__get_about_page();
        break;

    case 'get_categories':
        $response = ContentManager::__get_categories();
        break;

    case 'get_items':
        $response = ContentManager::__get_items( isset($_REQUEST['category_id']) && $_REQUEST['category_id'] ? $_REQUEST['category_id'] : null );
        break;

    case 'get_news':
        $response = ContentManager::__get_news();
        break;

    case 'get_contact_us':
        $response = ContentManager::__get_contact_us();
        break;

    default:
        $response = array(
            'result' => 'incorrect_action'
        );
        break;

}

echo json_encode($response);